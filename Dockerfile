# FROM fedora:31

# VOLUME /opt/robotframework/results
# VOLUME /opt/robotframework/tests

# RUN dnf upgrade -y && dnf install -y python37
# RUN dnf upgrade -y >/dev/null && echo OK
# RUN dnf install -y python37 >/dev/null && echo OK
# #RUN dnf install -y chromedriver-stable >/dev/null && echo OK
# RUN dnf install -y https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm >/dev/null && echo OK
# RUN chown root /usr/bin/chromedriver >/dev/null && echo OK
# RUN chmod +x /usr/bin/chromedriver >/dev/null && echo OK
# RUN chmod 755 /usr/bin/chromedriver >/dev/null && echo OK
# RUN pip3 install robotframework 
# RUN pip3 install robotframework-faker
# RUN pip3 install robotframework-requests==0.5.0 
# RUN pip3 install robotframework-seleniumlibrary 
# RUN pip3 install selenium==4.9.0| grep "Successfully installed"

FROM fedora:latest

VOLUME /opt/robotframework/results
VOLUME /opt/robotframework/tests

RUN dnf upgrade -y && dnf install -y python37 python3-pip
RUN dnf upgrade -y >/dev/null && echo OK
RUN dnf install -y python37 >/dev/null && echo OK
RUN dnf install -y https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
RUN dnf install -y chromedriver
RUN chown root /usr/bin/chromedriver >/dev/null && echo OK
RUN chmod +x /usr/bin/chromedriver >/dev/null && echo OK
RUN chmod 755 /usr/bin/chromedriver >/dev/null && echo OK


RUN pip3 install --upgrade pip
RUN pip3 install robotframework
RUN pip3 install robotframework-faker
RUN pip3 install robotframework-requests==0.5.0
RUN pip3 install robotframework-seleniumlibrary
RUN pip3 install selenium==4.9.0
